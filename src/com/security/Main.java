package com.security;

import java.math.BigInteger;

public class Main {

    public static void main(String[] args) {
        System.out.println("hello world\n");

        BigInteger p = PrimeNumber.getRandomPrimeNumber();
        BigInteger q = PrimeNumber.getRandomPrimeNumber();

        // Indicatrice d'Euler
        BigInteger m = (p.subtract(BigInteger.valueOf(1))).multiply(q.subtract(BigInteger.valueOf(1)));

        // Exposant public

        System.out.println("p : " + p);
        System.out.println("q : " + q);
        System.out.println("m : " + m);


        PrimeNumber.findFirstPrime(m);
    }
}
